//
//  JsonListVC.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit

class JsonListVC: UIViewController {

    @IBOutlet weak var mTable: UITableView!
    let mJSONDataSource = DataSource()
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDefinitionVC" {
            if let indexPath = getIndexPathForSelectedRow() {
                let record = mJSONDataSource.itemsInType(index: indexPath.section)[indexPath.row]
                let definitionVC = segue.destination as! DefinitionVC
                
                definitionVC.mRecord = record
            }
        }
        
    }
    
    // Our function to have a reference to indexPath for the TableView's selected row
    func getIndexPathForSelectedRow() -> IndexPath? {
        var indexPath: IndexPath?
        
        if mTable.indexPathsForSelectedRows!.count > 0 {
            indexPath = mTable.indexPathsForSelectedRows![0] as IndexPath
        }
        
        return indexPath
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mJSONDataSource.populate(type: "json")
    }
}

extension JsonListVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //print("sections = \(mJSONDataSource.numberOfCategories())")
        return mJSONDataSource.numberOfTypes()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mJSONDataSource.numberOfItemsInEachType(index: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        
        let records: [Record] = mJSONDataSource.itemsInType(index: indexPath.section)
        let record = records[indexPath.row]
        
        cell.textLabel?.text = record.name.capitalized
        
        return cell
    }
    
    
    /*func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     return mJSONDataSource.getSectionLabelAtIndex(index: section)
     }*/
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label : UILabel = UILabel()
        
        label.text = mJSONDataSource.getTypeLabelAtIndex(index: section)
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 20.0)
        label.textAlignment = .center
        // Color Literal
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.backgroundColor = #colorLiteral(red: 0.5791940689, green: 0.1280144453, blue: 0.5726861358, alpha: 1)
        
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    }

