//
//  CoreDataHelpers.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 8.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit
import Foundation
import CoreData

    class CoreDataHelpers: UIViewController {

        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        static let shared = CoreDataHelpers()
        func save(object: [String: Any]){
            let entity = NSEntityDescription.insertNewObject(forEntityName: "Entity", into: context)
            as! Entity
            
            entity.name = object["name"] as? String ?? ""
            entity.type = object["type"] as? String ?? ""
            entity.example = object["example"] as? String ?? ""
            
            do {
                try context.save()
            }catch{
                print("Data is not saved")
            }
            
        }
        
        func getAllData() -> [Entity]{
            var entity:[Entity] = []
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Entity")
            do{
                entity = try context.fetch(fetchRequest) as! [Entity]
            }catch{
                print("Not get data")
            }
            return entity
        }
        
        func deleteAtIndex(index: Int) -> [Entity]{
               var entity = getAllData()
               context.delete(entity[index])
               entity.remove(at: index)
               do{
                   try context.save()
               }catch{
                   print("Data not save")
               }
               return entity
           }
        
        func updateData(object: [String: Any], index: Int){
            let entity = getAllData()
            entity[index].name = object["word"] as? String ?? ""
            entity[index].type = object["type"] as? String ?? ""
            entity[index].example = object["example"] as? String ?? ""
            do{
                try context.save()
            }catch{
                print("Data not save")
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
        }
        

        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }
