//
//  UserRecordsVC.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit
import CoreData

class UserRecordsVC: UIViewController {
    
    var mEntity = [Entity]()
    @IBOutlet weak var mTable: UITableView!
    var index = 0

    @objc func addNewItem() {
         let mAlert = UIAlertController(title: "Enter Name & Surname",
                                        message: "Enter Text",
                                        preferredStyle: .alert)
         var wordTextField: UITextField?
         mAlert.addTextField {
             (textField) -> Void in
             wordTextField = textField
             textField.placeholder = "word"
         }
         
         var typeTextField: UITextField?
         mAlert.addTextField {
             (textField) -> Void in
             typeTextField = textField
             textField.placeholder = "surname"
         }
         
         var exampleTextField: UITextField?
         mAlert.addTextField {
             (textField) -> Void in
             exampleTextField = textField
             textField.placeholder = "midterm"
         }
         
         mAlert.addAction(UIAlertAction(title: "Ok",
                style: .default,
                handler: { (action) -> Void in
                 // If the name and surname are not empty then the data will be inserted (midterm and final are not checked!!!)
                 if wordTextField!.text!.isEmpty || typeTextField!.text!.isEmpty {
                     print("Empty Fields")
                     
                 }
                 else {
                     let word = wordTextField!.text!
                     let type = typeTextField!.text!
                    let example = exampleTextField!.text!
                     let dic = ["name": word, "type": type, "example": example] as [String : Any]
                    CoreDataHelpers.shared.save(object: dic)
                 }
                 
                 self.mTable.reloadData()
         }))
         
         self.present(mAlert, animated: true, completion: nil)
     }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mEntity = CoreDataHelpers.shared.getAllData()
        mTable.reloadData()
    }    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserRecordsVC:  UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Recommended way
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! Cell
            
        // Get the Student for this index
        let entity = mEntity[indexPath.row]
        cell.wordLbl.text = entity.name!
        cell.typeLbl.text = entity.type!
        cell.exampleLbl.text = entity.example!
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mEntity.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        self.performSegue(withIdentifier: "update", sender: self)
    }
        
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.mEntity = CoreDataHelpers.shared.deleteAtIndex(index: indexPath.row)
            self.mTable.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

class Cell : UITableViewCell{
    @IBOutlet weak var wordLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var exampleLbl: UILabel!
    
}
