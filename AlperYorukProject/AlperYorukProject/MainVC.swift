//
//  MainVC.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var infoBtn: UIButton!
    
    @IBAction func unwindToMain(_ sender: UIStoryboardSegue) {
        
    }
    
    @IBAction func infoBtnDoubleTap(_ sender: UITapGestureRecognizer) {
        let mAlert = UIAlertController(title: "Word Learner", message: "A project made for CTIS480.", preferredStyle: .alert)
        
        mAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        
        self.present(mAlert, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
