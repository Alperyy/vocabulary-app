//
//  DataSource.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import Foundation

class DataSource {

    var mRecordList: [Record] = []
    var types: [String] = []
    
    func numberOfItemsInEachType(index: Int) -> Int {
        return itemsInType(index: index).count
    }
    
    func numberOfTypes() -> Int {
        return types.count
    }
    
    func getTypeLabelAtIndex(index: Int) -> String {
        return types[index]
    }
    
    // MARK:- Populate Data from files
    
    func populate(type: String) {
        if type.lowercased() == "json" {
            if let path = Bundle.main.path(forResource: "words", ofType: "json") {
                if let jsonToParse = NSData(contentsOfFile: path) {
                    
                    // https://www.dotnetperls.com/guard-swift
                    guard let json = try? JSON(data: jsonToParse as Data) else {
                        print("Error with JSON")
                        return
                    }
                    //print(json)
                    
                    for index in 0..<json["words"].count {
                        let name = json["words"][index]["name"].string!
                        let type = json["words"][index]["type"].string!
                        let description = json["words"][index]["description"].string!
                        
                        let mRecord = Record(name: name, type: type, description: description)
                        mRecordList.append(mRecord)
                        
                        if !types.contains(type) {
                            types.append(type)
                        }
                    }
                }
                else {
                    print("NSData error")
                }
            }
            else {
                print("Path error")
            }
        }
        else {

        }
    }


    // MARK:- itemsForEachGroup
    
    func itemsInType(index: Int) -> [Record] {
        let item = types[index]
        
        // See playground6 for Closure
        // http://locomoviles.com/uncategorized/filtering-swift-array-dictionaries-object-property/
        
        let filteredItems = mRecordList.filter { (record: Record) -> Bool in
            return record.type == item
        }
        return filteredItems
    }
}
