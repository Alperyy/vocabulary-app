//
//  Record.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import Foundation

class Record {

    var type: String
    var name: String
    var description: String
    
    init(name: String, type: String, description: String) {
        self.type = type
        self.name = name
        self.description = description
    }

}
