//
//  DefinitionVC.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit

class DefinitionVC: UIViewController {
    
    var mRecord: Record?
    
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mType: UILabel!
    @IBOutlet weak var mDefinition: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let tempRecord = mRecord {
            navigationItem.title = tempRecord.name.capitalized
            
            mName.text = tempRecord.name
            mType.text = tempRecord.type
            mDefinition.text = tempRecord.description
        }
    }
}
