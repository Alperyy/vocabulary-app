//
//  AddRecordVC.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 4.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//

import UIKit
import AVFoundation
import TextFieldEffects

class AddRecordVC: UIViewController {
    
    @IBOutlet weak var mTitle: UILabel!
    @IBOutlet weak var wordTF: UITextField!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var exampleTF: UITextField!
    
    var audioPlayer: AVAudioPlayer!

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func addBtnPressed(_ sender: UIButton) {	
        
        if(wordTF.text == "" || typeTF.text == "" || exampleTF.text == "" ){
            let mAlert = UIAlertController(title: "Oops!!", message: "Please fill all the fields!", preferredStyle: .alert)
            mAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(mAlert, animated: true, completion: nil)
        }
        else{
            let word = wordTF.text!
            let type = typeTF.text!
            let example = exampleTF.text!
            let dic = ["name": word, "type": type, "example": example] as [String : Any]
            CoreDataHelpers.shared.save(object: dic)
            clearTextField()
            
            if let soundURL = Bundle.main.url(forResource: "apple_success_sound", withExtension: "wav") {
            
                do {
                    audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
                }
                catch {
                    print(error)
                }
                
                audioPlayer.play()
            }else{
                print("Unable to locate audio file")
            }
        }
    }
    
    func clearTextField(){
        wordTF.text = ""
        typeTF.text = ""
        exampleTF.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
