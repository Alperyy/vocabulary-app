//
//  Entity+CoreDataProperties.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 8.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//
//

import Foundation
import CoreData


extension Entity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest<Entity>(entityName: "Entity")
    }

    @NSManaged public var type: String?
    @NSManaged public var example: String?
    @NSManaged public var name: String?

}
