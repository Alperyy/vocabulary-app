//
//  Entity+CoreDataClass.swift
//  AlperYorukProject
//
//  Created by CTIS Student on 8.06.2020.
//  Copyright © 2020 CTIS. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Entity)
public class Entity: NSManagedObject {
    
    // Static method (class keyword)
    class func createInManagedObjectContext(_ context: NSManagedObjectContext, name: String, type: String, example: String) -> Entity {
        let entityObject = NSEntityDescription.insertNewObject(forEntityName: "Entity", into: context) as! Entity
        entityObject.name = name
        entityObject.type = type
        entityObject.example = example
        
        return entityObject
    }
    
}
